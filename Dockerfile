FROM gitlab-registry.cern.ch/push-notifications/email-gateway/email-gateway-base:0d215bba
ARG build_env

WORKDIR /opt/
COPY ./ ./

RUN python -V && \
    pip -V && \
    pip install --upgrade pip && \
    pip install poetry && \
    poetry config virtualenvs.create false

RUN if [ "$build_env" == "development" ]; \
    then poetry install; \
    else poetry install --no-dev; \
    fi

CMD ["python",  "-m", "email_gateway"]
