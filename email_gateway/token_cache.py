"""Token Caching in Etcs definition."""
import json
import logging

from etcd3 import Client
from etcd3.errors import ErrInvalidAuthToken

from email_gateway.config import Config


class TokenCache:
    """Cache Oauth2 token to etcd."""

    def __init__(self, conf: Config) -> None:
        """Initialize the etcd connection and cache settings."""
        self.client = None
        self.conf = conf
        if self.conf.AUDITING:
            self.client = Client(host=self.conf.ETCD_HOST, port=self.conf.ETCD_PORT)
        if self.client and self.conf.ETCD_USER:
            self.client.auth(username=self.conf.ETCD_USER, password=self.conf.ETCD_PASSWORD)

    def put_refresh_token_cache(self, refresh_token, app_name="Generic"):
        """Put refresh_token for 365 in etcd."""
        def put():
            self.client.put(
                f"/365/{app_name}/refresh_token",
                json.dumps(refresh_token, default=str),
            )
            logging.info("refresh_token saved")

        try:
            put()
        except ErrInvalidAuthToken:
            logging.debug("refresh etcd token")
            self.client.auth(username=self.conf.ETCD_USER, password=self.conf.ETCD_PASSWORD)
            put()
        except Exception:
            logging.exception("Error put_refresh_token_cache to etcd3:")

    def get_refresh_token_cache(self, app_name="Generic"):
        """Get refresh_token for 365 from etcd."""

        def get():
            if self.client:
                kvs = self.client.range(f"/365/{app_name}/refresh_token").kvs
                if kvs:
                    return json.loads(kvs[0].value)
            return None

        try:
            return get()
        except ErrInvalidAuthToken:
            logging.debug("refresh etcd token")
            self.client.auth(username=self.conf.ETCD_USER, password=self.conf.ETCD_PASSWORD)
            return get()
        except Exception:
            logging.exception("Error get_refresh_token_cache from etcd")
            return None
