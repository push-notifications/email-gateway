"""Email Gateway Entrypoint."""
from email_gateway import app


if __name__ == "__main__":
    app.run()
