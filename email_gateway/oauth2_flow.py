"""Process Oauth2 authentication flow."""

import sys
import json
import logging

import msal
import time

from email_gateway.config import Config
from email_gateway.token_cache import TokenCache


class Oauth2Flow:
    """Process Oauth2 authentication flow."""

    def __init__(self, conf: Config) -> None:
        """Initialize the Oauth2 Flow."""
        self.conf = conf
        self.result = None
        self.cache = msal.SerializableTokenCache()
        self.tokencache = TokenCache(self.conf)

    def init_token_cache(self):
        """Init token cache."""
        # Get cached token from DB if any
        refresh_token = self.tokencache.get_refresh_token_cache("email_gateway")
        if refresh_token:
            self.cache.deserialize(refresh_token)
            logging.debug("refresh_token cache loaded")

    def get_access_token(self) -> "tuple[str, str, float]":
        """Create a preferably long-lived app instance which maintains a token cache."""
        app = msal.PublicClientApplication(
            self.conf.AZURE_CLIENT_ID,
            authority=self.conf.AZURE_AUTHORITY,
            token_cache=self.cache
        )

        result = None
        # Try to reload token from the cache
        accounts = app.get_accounts()
        if accounts:
            result = app.acquire_token_silent(
                scopes=self.conf.AZURE_SCOPE,
                account=accounts[0],
                authority=None,
                force_refresh=False,
                claims_challenge=None,
            )

        if not result:
            logging.info("No suitable token exists in cache. Let's get a new one from AAD.")

            flow = app.initiate_device_flow(scopes=self.conf.AZURE_SCOPE)
            if "user_code" not in flow:
                raise ValueError("Fail to create device flow. Err: %s" % json.dumps(flow, indent=4))

            # Display the MS Device flow authentication url on the console
            print(flow["message"])
            sys.stdout.flush()  # Some terminal needs this to ensure the message is shown
            # Send the MS Device flow authentication url to Sentry as an error
            logging.error(flow["message"])

            # Wait here for the Browser authentication flow to be done
            result = app.acquire_token_by_device_flow(flow)  # By default it will block

        if "access_token" in result:
            # return the access token AND the username
            if not accounts:
                accounts = app.get_accounts()
            logging.debug("Token acquired for: %s", accounts[0]["username"])

            # Save refresh_token to cache
            if self.cache.has_state_changed:
                self.tokencache.put_refresh_token_cache(self.cache.serialize(), "email_gateway")
                logging.debug("refresh_token cache saved")

            return result["access_token"], accounts[0]["username"], time.time() + result["expires_in"]

        else:
            raise ValueError(
                "Error getting access_token",
                result.get("error"),
                result.get("error_description"),
                result.get("correlation_id"),
            )
