"""Configuration definition."""
import ast
import os


ENV_DEV = "development"
ENV_PROD = "production"


class Config:
    """App configuration."""

    ENV = os.getenv("ENV", ENV_DEV)
    DEBUG = ast.literal_eval(os.getenv("DEBUG", "False"))
    LOGGING_CONFIG_FILE = os.getenv("LOGGING_CONFIG_FILE", "logging.yaml")

    # Imap
    SERVER = os.getenv("SERVER")
    EMAIL = os.getenv("EMAIL")
    # Only needed for IMAP_AUTHENTICATION_MODE=password
    PASSWORD = os.getenv("PASSWORD")
    # "password" or "xoauth2" or "xoauth2graph"
    IMAP_AUTHENTICATION_MODE = os.getenv("IMAP_AUTHENTICATION_MODE", "password")

    # Only needed for IMAP_AUTHENTICATION_MODE=xoauth2 or xoauth2graph
    # 365 IMAP Authentication
    # cern domain cern.ch
    # use common is the app targeting also external users
    AZURE_AUTHORITY = "https://login.microsoftonline.com/cern.ch"
    AZURE_CLIENT_ID = "543725ae-fd38-436a-a717-009b1a8137be"

    # for IMAP_AUTHENTICATION_MODE=xoauth2
    # AZURE_SCOPE = [
    #     "https://outlook.office.com/IMAP.AccessAsUser.All",
    # ]
    # for IMAP_AUTHENTICATION_MODE=xoauth2graph
    # AZURE_SCOPE = [
    #     "https://graph.microsoft.com/.default",
    # ]

    AZURE_SCOPE = os.getenv("AZURE_SCOPE", "https://outlook.office.com/IMAP.AccessAsUser.All").split(",")

    GRAPHAPI_BASEURL = "https://graph.microsoft.com/v1.0"

    # AZURE_TOKEN_REFRESH = os.getenv("AZURE_TOKEN_REFRESH")

    # IMAP filter: ALL / "NOT (TO \"critical\")" / "TO \"critical\""
    SEARCH_CRITERIA = os.getenv("SEARCH_CRITERIA", "ALL")
    # GRAPH filter: ALL / "critical" / "not critical"
    SEARCH_CRITERIA_GRAPH = os.getenv("SEARCH_CRITERIA_GRAPH", "ALL")

    ARCHIVE_FOLDER = os.getenv("ARCHIVE_FOLDER", "Archive")
    BULK_SIZE = int(os.getenv("BULK_SIZE", "10"))
    BULK_INTERVAL = int(os.getenv("BULK_INTERVAL", "60"))
    RECONNECT_INTERVAL = int(os.getenv("RECONNECT_INTERVAL", "600"))

    # Sentry
    SENTRY_DSN = os.getenv("SENTRY_DSN")

    # ActiveMQ
    PUBLISHER_NAME = os.getenv("PUBLISHER_NAME", "email_gateway_publisher")
    EXTENSION = os.getenv("EXTENSION")

    # Etcd auditing
    ETCD_HOST = os.getenv("ETCD_HOST", "localhost")
    ETCD_PORT = os.getenv("ETCD_PORT", 2379)
    AUDITING = os.getenv("AUDITING", False)
    ETCD_USER = os.getenv("ETCD_USER", None)
    ETCD_PASSWORD = os.getenv("ETCD_PASSWORD", None)
    AUDIT_ID = os.getenv("AUDIT_ID", "email_gateway")


class DevelopmentConfig(Config):
    """Development configuration overrides."""

    ENV = os.getenv("ENV", ENV_DEV)
    DEBUG = ast.literal_eval(os.getenv("DEBUG", "True"))


class ProductionConfig(Config):
    """Production configuration overrides."""

    ENV = os.getenv("ENV", ENV_PROD)
    DEBUG = ast.literal_eval(os.getenv("DEBUG", "False"))


def load_config():
    """Load the configuration."""
    config_options = {ENV_DEV: DevelopmentConfig, ENV_PROD: ProductionConfig}
    environment = os.getenv("ENV", ENV_DEV)

    return config_options[environment]
