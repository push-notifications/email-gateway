"""Email Gateway app definition and creation."""
import asyncio
import logging
import logging.config as logging_config
import yaml
import time
from imap_tools import MailBox
from megabus import Publisher

from email_gateway.config import load_config
from email_gateway.email_gateway import EmailGateway
from email_gateway.email_gateway_graph import EmailGatewayGraph
from email_gateway.oauth2_flow import Oauth2Flow


class App:
    """Email Gateway app."""

    def __init__(self, conf):
        """Initialize the App."""
        self.conf = conf
        self.oauth = Oauth2Flow(conf)

    async def start(self):
        """Init all dependencies and start processing emails."""
        publisher = Publisher(self.conf.PUBLISHER_NAME)
        logging.info("Finished initializing...")

        if self.conf.IMAP_AUTHENTICATION_MODE == "password":
            await self.run_with_legacy_mailbox(publisher)
            return

        if self.conf.IMAP_AUTHENTICATION_MODE == "xoauth2":
            self.oauth.init_token_cache()
            await self.run_with_oauth_mailbox(publisher)
            return

        if self.conf.IMAP_AUTHENTICATION_MODE == "xoauth2graph":
            self.oauth.init_token_cache()
            await self.run_with_oauth_graph(publisher)
            return

        raise ValueError("Invalid IMAP_AUTHENTICATION_MODE: %s" % self.conf.IMAP_AUTHENTICATION_MODE)

    async def run_with_legacy_mailbox(self, publisher: Publisher):
        """Read emails in loop with password authentication - legacy flow."""
        logging.info("IMAP with password authentication to %s.", self.conf.SERVER)
        with MailBox(self.conf.SERVER).login(self.conf.EMAIL, self.conf.PASSWORD) as mailbox:
            while True:
                await EmailGateway(self.conf, publisher, mailbox).process_emails()
                await asyncio.sleep(self.conf.BULK_INTERVAL)

    async def run_with_oauth_mailbox(self, publisher: Publisher):
        """Read emails in loop with oauth authentication."""
        logging.info("IMAP with XOAUTH2 authentication to %s.", self.conf.SERVER)
        while True:
            # Authenticate to account using OAuth 2.0 mechanism
            access_token, username, expiration_time = self.oauth.get_access_token()
            with MailBox(self.conf.SERVER).xoauth2(username, access_token) as mailbox:
                tcurrent = time.time()
                logging.info("access_token will expire at %s",
                             time.strftime("%H:%M:%S", time.localtime(expiration_time)))
                # Stay connected until token expires (with 60s security)
                while (expiration_time - tcurrent) > 60:
                    await EmailGateway(self.conf, publisher, mailbox).process_emails()
                    await asyncio.sleep(self.conf.BULK_INTERVAL)
                    tcurrent = time.time()
            # Exit and reconnect

    async def run_with_oauth_graph(self, publisher: Publisher):
        """Read emails in graphAPI loop with oauth authentication."""
        logging.info("Graph with XOAUTH2 authentication.")
        while True:
            # Authenticate to account using OAuth 2.0 mechanism
            access_token, username, expiration_time = self.oauth.get_access_token()
            email_gateway_graph = EmailGatewayGraph(self.conf, publisher, access_token, username)
            email_gateway_graph.create_folder(self.conf.ARCHIVE_FOLDER)
            tcurrent = time.time()
            logging.info("access_token will expire at %s", time.strftime("%H:%M:%S", time.localtime(expiration_time)))
            # Stay connected until token expires (with 60s security)
            while (expiration_time - tcurrent) > 60:
                email_gateway_graph.process_emails()
                await asyncio.sleep(self.conf.BULK_INTERVAL)
                tcurrent = time.time()
            # Exit and reconnect

    def run(self):
        """Run the app in a loop."""
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.start())
        loop.close()


def configure_logging(conf):
    """Configure logs."""
    with open(conf.LOGGING_CONFIG_FILE, "r") as file:
        logging_config.dictConfig(yaml.safe_load(file.read()))


def create_app():
    """Create a new App."""
    conf = load_config()
    if conf.SENTRY_DSN:
        import sentry_sdk

        sentry_sdk.init(dsn=conf.SENTRY_DSN)

    configure_logging(conf)

    return App(conf)


def run():
    """Run App."""
    create_app().run()
