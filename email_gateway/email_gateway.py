"""Process incoming emails."""
import json
import logging
import re

from imap_tools import MailBox, MailMessage
from megabus import Publisher
from textile import textile

from email_gateway.config import Config


class EmailGateway:
    """Process incoming emails."""

    def __init__(self, conf: Config, publisher: Publisher, mailbox: MailBox) -> None:
        """Initialize the Email Gateway."""
        self.conf = conf
        self.__publisher = publisher
        self.__mailbox = mailbox

    async def process_emails(self) -> None:
        """Process emails and forward to queue."""
        if not self.__mailbox.folder.exists(self.conf.ARCHIVE_FOLDER):
            self.__mailbox.folder.create(self.conf.ARCHIVE_FOLDER)
        logging.debug("Fetching emails...")
        self.__process_bulk()

    def __process_bulk(self) -> None:
        for mail in self.__mailbox.fetch(criteria=self.conf.SEARCH_CRITERIA, limit=self.conf.BULK_SIZE, bulk=True):
            self.__parse_headers_and_process(mail)
            self.__mailbox.move([mail.uid], self.conf.ARCHIVE_FOLDER)

    def __parse_headers_and_process(self, mail: MailMessage) -> None:
        if not mail.to:
            logging.warning("Failed email processing")
            return

        mail_to = mail.to[0]
        list_id = None

        # Proper egroup sending, ListID is present
        is_from_mailing_list = mail.headers.get("List-ID") and mail.headers.get("Delivered-To")
        if is_from_mailing_list:
            mail_to = mail.headers["Delivered-To"][0]
            if "@" not in mail_to:
                mail_to += "@cern.ch"
            list_id = mail.headers["List-ID"][0].strip(" <>").replace(".cern.ch", "@cern.ch")
            if "@" not in list_id:
                list_id += "@cern.ch"
        # No ListID let's try to identify if it's an egroup anyway
        # Could be coming from a Mail Team migrated egroup in AD (relayed to the channel email contact directly)
        elif mail.headers.get("Delivered-To"):
            # If delivered-to and mail-to are different that could be an egroup
            tentative_mail_to = mail.headers["Delivered-To"][0]
            if "@" not in tentative_mail_to:
                tentative_mail_to += "@cern.ch"
            if mail_to != tentative_mail_to:
                # Check if mailto looks like an egroup and is not a channel email
                if re.match(r"\w+-\w+@cern.ch", mail_to):
                    list_id = mail_to
                    mail_to = tentative_mail_to

        content = mail.html
        if not content:
            content = textile(mail.text)

        self.__process_one(mail.uid, mail.from_, mail.subject, mail_to, content, list_id)

    def __process_one(self, uid: str, sender: str, subject: str, to: str, content: str, list_id: str) -> None:
        logging.info("Uid: %s From: %s Subject: %s To: %s ListId: %s", uid, sender.lower(), subject, to, list_id)

        # Lowercase from to prevent case compare issues later in the flow
        message = json.dumps(
            {"sender": sender.lower(), "subject": subject, "content": content, "to": to, "listid": list_id}
        )
        self.__publisher.send(message, extension=self.conf.EXTENSION, headers={"persistent": "true"})
