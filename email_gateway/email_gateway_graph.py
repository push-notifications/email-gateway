"""Process incoming emails."""
import json
import logging
import requests

from megabus import Publisher
from textile import textile

from email_gateway.config import Config


class EmailGatewayGraph:
    """Process incoming emails with Graph API."""

    def __init__(self, conf: Config, publisher: Publisher, access_token: str, username: str) -> None:
        """Initialize the Email Gateway."""
        self.conf = conf
        self.__publisher = publisher
        self.__username = username
        self.__access_token = access_token

    def process_emails(self) -> None:
        """Process emails and forward to queue."""
        # self.create_folder(self.conf.ARCHIVE_FOLDER)
        logging.debug("Fetching emails...")
        self.__process_bulk()

    def __process_bulk(self) -> None:
        for mail in self.__fetch_messages(folder_name="Inbox", limit=self.conf.BULK_SIZE, bulk=True):
            if self.__parse_headers_and_process(mail):
                self.__mark_message_read(mail["id"])
                self.__move_message(mail["id"], self.conf.ARCHIVE_FOLDER)

    def __parse_headers_and_process(self, mail) -> None:
        if (
            "toRecipients" not in mail
            or len(mail["toRecipients"]) == 0
            or "emailAddress" not in mail["toRecipients"][0]
            or "address" not in mail["toRecipients"][0]["emailAddress"]
        ):
            logging.warning("Failed email processing, to missing or invalid")
            return False

        mail_to = mail["toRecipients"][0]["emailAddress"]["address"]
        if self.conf.SEARCH_CRITERIA_GRAPH == "critical":
            if "critical@" not in mail_to:
                logging.debug("Skipping NON critical mail sent to %s", mail_to)
                return False
        if self.conf.SEARCH_CRITERIA_GRAPH == "not critical":
            if "critical@" in mail_to:
                logging.debug("Skipping critical mail sent to %s", mail_to)
                return False

        list_id = None

        # !!! Not working anymore since the move to 365. Both headers are missing. !!!
        # # Proper egroup sending, ListID is present
        # is_from_mailing_list = mail.headers.get("List-ID") and mail.headers.get("Delivered-To")
        # if is_from_mailing_list:
        #     mail_to = mail.headers["Delivered-To"][0]
        #     if "@" not in mail_to:
        #         mail_to += "@cern.ch"
        #     list_id = mail.headers["List-ID"][0].strip(" <>").replace(".cern.ch", "@cern.ch")
        #     if "@" not in list_id:
        #         list_id += "@cern.ch"
        # # No ListID let's try to identify if it's an egroup anyway
        # # Could be coming from a Mail Team migrated egroup in AD (relayed to the channel email contact directly)
        # elif mail.headers.get("Delivered-To"):
        #     # If delivered-to and mail-to are different that could be an egroup
        #     tentative_mail_to = mail.headers["Delivered-To"][0]
        #     if "@" not in tentative_mail_to:
        #         tentative_mail_to += "@cern.ch"
        #     if mail_to != tentative_mail_to:
        #         # Check if mailto looks like an egroup and is not a channel email
        #         if re.match(r"\w+-\w+@cern.ch", mail_to):
        #             list_id = mail_to
        #             mail_to = tentative_mail_to
        # logging.debug("Mail: %s", mail)

        # Work with uniqueBody if available (should be always)
        # uniqueBody contains only the mail, not the previous replies and/or threads
        if "uniqueBody" in mail:
            tmp_body = mail["uniqueBody"]
        else:
            tmp_body = mail["body"]
        if tmp_body["contentType"] == "text":
            content = textile(tmp_body["content"])
        else:
            content = tmp_body["content"]

        self.__process_one(
            mail["id"], mail["from"]["emailAddress"]["address"], mail["subject"], mail_to, content, list_id
        )
        return True

    def __process_one(self, uid: str, sender: str, subject: str, to: str, content: str, list_id: str) -> None:
        logging.info("Uid: %s From: %s Subject: %s To: %s ListId: %s", uid, sender.lower(), subject, to, list_id)

        # Lowercase from to prevent case compare issues later in the flow
        message = json.dumps(
            {"sender": sender.lower(), "subject": subject, "content": content, "to": to, "listid": list_id}
        )
        self.__publisher.send(message, extension=self.conf.EXTENSION, headers={"persistent": "true"})

    def __fetch_messages(self, folder_name: str, limit: int, bulk: bool):
        """Return a list of messages in the specified folder."""
        # folder_id = self._find_folder_id_from_folder_path(folder_name)
        # If target folder is a well known folder, no need to get the ID.
        folder_id = folder_name
        request_url = f"{self.conf.GRAPHAPI_BASEURL}/users/{self.__username}/mailFolders/{folder_id}/messages"
        batch_size = limit
        if not bulk:
            batch_size = 0
        messages: list
        params = {"$select": "id,from,sender,toRecipients,subject,body,uniqueBody"}
        if batch_size and batch_size > 0:
            params["$top"] = batch_size
        else:
            params["$top"] = 100
        result = requests.get(
            request_url,
            headers={
                "Authorization": "Bearer " + self.__access_token,
                "Content-type": "application/json",
                "Prefer": 'outlook.body-content-type="html"',
            },
            params=params,
        )
        if result.status_code != 200:
            raise RuntimeError(f"Failed to fetch messages {result.text}")
        messages = result.json()["value"]
        # Loop if next page is present and not obtained message limit.
        while "@odata.nextLink" in result.json() and (batch_size == 0 or batch_size - len(messages) > 0):
            result = requests.get(
                result.json()["@odata.nextLink"],
                headers={"Authorization": "Bearer " + self.__access_token, "Content-type": "application/json"},
            )
            if result.status_code != 200:
                raise RuntimeError(f"Failed to fetch messages {result.text}")
            messages.extend(result.json()["value"])
        return messages

    def _find_folder_id_with_parent(self,
                                    folder_name: str,
                                    parent_folder_id: str = None):
        sub_url = ''
        if parent_folder_id is not None:
            sub_url = f'/{parent_folder_id}/childFolders'
        url = f"{self.conf.GRAPHAPI_BASEURL}/users/{self.__username}/mailFolders{sub_url}"
        filter = f"?$filter=displayName eq '{folder_name}'"
        folders_resp = self._client.get(url + filter)
        folders_resp = requests.get(
            url,
            headers={
                "Authorization": "Bearer " + self.__access_token,
                "Content-type": "application/json",
            }
        )
        if folders_resp.status_code != 200:
            raise RuntimeWarning(f"Failed to list folders."
                                 f"{folders_resp.json()}")
        folders: list = folders_resp.json()['value']
        matched_folders = [folder for folder in folders
                           if folder['displayName'] == folder_name]
        if len(matched_folders) == 0:
            raise RuntimeError(f"folder {folder_name} not found")
        selected_folder = matched_folders[0]
        return selected_folder['id']

    def create_folder(self, folder_name: str):
        """Create a folder."""
        sub_url = ""
        path_parts = folder_name.split("/")
        if len(path_parts) > 1:  # Folder is a subFolder
            parent_folder_id = None
            for folder in path_parts[:-1]:
                parent_folder_id = self._find_folder_id_with_parent(folder, parent_folder_id)
            sub_url = f"/{parent_folder_id}/childFolders"
            folder_name = path_parts[-1]

        request_body = {"displayName": folder_name}
        request_url = f"{self.conf.GRAPHAPI_BASEURL}/users/{self.__username}/mailFolders{sub_url}"
        resp = requests.post(
            request_url,
            headers={"Authorization": "Bearer " + self.__access_token, "Content-type": "application/json"},
            json=request_body,
        )
        if resp.status_code == 409:
            logging.debug("Folder %s already exists, skipping creation", folder_name)
            return

        if resp.status_code == 201:
            logging.debug("Created folder %s", folder_name)
            return

        logging.warning("create_folder unknown response %s: %s", resp.status_code, resp.json())

    def __move_message(self, message_id: str, folder_name: str):
        """Move a message to another folder."""
        # folder_id = self._find_folder_id_from_folder_path(folder_name)
        # If target folder is a well known folder, no need to get the ID.
        # 'Archive' is one of them.
        folder_id = folder_name
        request_body = {"destinationId": folder_id}
        request_url = f"{self.conf.GRAPHAPI_BASEURL}/users/{self.__username}/messages/{message_id}/move"
        resp = requests.post(
            request_url,
            headers={"Authorization": "Bearer " + self.__access_token, "Content-type": "application/json"},
            json=request_body,
        )
        if resp.status_code != 201:
            raise RuntimeWarning(f"Failed to move message " f"{resp.status_code}: {resp.json()}")

    def __mark_message_read(self, message_id: str):
        """Mark a message as read."""
        request_url = f"{self.conf.GRAPHAPI_BASEURL}/users/{self.__username}/messages/{message_id}"
        resp = requests.patch(
            request_url,
            headers={"Authorization": "Bearer " + self.__access_token, "Content-type": "application/json"},
            json={"isRead": "true"},
        )
        if resp.status_code != 200:
            raise RuntimeWarning(f"Failed to mark message read" f"{resp.status_code}: {resp.json()}")
