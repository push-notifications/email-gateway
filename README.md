# Email Gateway

## Contribute

* Locally clone the repository

```bash
$ git clone ssh://git@gitlab.cern.ch:7999/push-notifications/email-gateway.git
$ cd email-gateway
```

* Make sure you have the latest version

```bash
$ git checkout master
$ git pull
```

* Install [pre-commit](https://pre-commit.com/#install) hooks

# Develop with docker-compose

This is the recommended method to develop.

### Dependencies
- [Docker](https://docs.docker.com/engine/install/)
- [Docker-compose](https://docs.docker.com/compose/install/)

### Instructions

A dummy mail server, with a dummy user and mailbox, has been configured with docker-compose, in order to allow faster development.
If you check the `.env` values you can verify what is the configured user and password.
No changes are needed in order to run it, just match sent mails to the configured user/mailbox.

- Login into cern's gitlab registry, to be able to load the docker images:
```bash
$ docker login gitlab-registry.cern.ch
```

- Start the consumer container and ActiveMQ and local email server
```bash
$ make docker-build-env
```

- There's also a utility to send an email to the local server. For that open a new terminal and enter the email-gateway container:
```bash
$ make docker-shell-env  # To enter the container
$ make send-email # To send one email notification, when already inside the container
```

Alternatively run directly inside of the container:
```bash
$ python scripts/send-email.py  # to test normal email setup or
$ python scripts/send-email-mailing-list.py  # to test mailing lists
```

To test the setup, it is possible to see the message in [ActiveMQ's admin UI](http://localhost:8161/admin)

- Clean up the containers
```bash
$ make docker-stop
```

- Force recreate of images
```bash
$ make docker-rebuild-env
```

### Generic Guidelines

* Develop, test and finish the new feature with regular commits

```bash
$ git add file1 file2
$ git commit
...
$ git push
```

* When ready, rebase interactively against the latest `master` to tidy things up and then create a merge request

```bash
$ git checkout master
$ git pull
$ git checkout dev_short_feature_description
$ git rebase -i master
$ git push
```

[Create a merge request](https://gitlab.cern.ch/push-notifications/email-gateway/-/merge_requests) from the `dev` branch into `master`. Assign a reviewer. Following a discussion and approval from the reviewer, the merge request can be merged.


### Manage dependencies
We're using [poetry](https://python-poetry.org/docs/cli/) to manage dependencies


- Regenerate lock after manually changing `pyproject.toml`:
```bash
poetry lock
```

- Add a dependency with:
```bash
poetry add "pendulum~2.0.5"
```
See more on choosing [dependency constrains](https://python-poetry.org/docs/versions/).

- Update a dependency:
```bash
poetry update requests
```

# Graph API vs IMAP

Graph API is the recommended approach to work with Outlook mailboxes.
One key difference though is support for filtering.
Current implementation of IMAP allows query+filtering based on critical level of a separate deployment, which guarantees even on a high influx of emails critical ones are processed with priority.
Graph API implementation differs here, because it  loads all emails into memory, and while iterating through them, then will identify those critical allowing priority in processing only on the following steps.
For the time being this feature is not needed, if ever it becomes then implementation and choice of API needs to be revisited.