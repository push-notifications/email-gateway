#################################################################################
## Usage:
##
## make setup-env           # sets up the environment
## make lint 	            # runs linting tools outside docker
## make docker-build 	    # builds docker image
## make ci-lint 	        # runs linting tools inside docker
## make docker-build-env    # run docker-compose: creates images, containers, volumes and start the email-gateway
## make docker-rebuild-env  # force create of docker environment
## make docker-shell-env    # bash the main container
## make docker-run          # run a email-gateway
##
#################################################################################

setup-env:
	poetry install
.PHONY: setup-env

lint:
	flake8
.PHONY: lint

docker-build:
	docker build -t email-gateway . --no-cache --build-arg build_env=development
.PHONY: docker-build

ci-lint:
	docker run email-gateway make lint
.PHONY: ci-lint

docker-build-env:
	docker-compose up --remove-orphans
.PHONY: docker-build-env

docker-rebuild-env:
	docker-compose build --force-rm --pull --no-cache
	docker-compose up --force-recreate --remove-orphans
.PHONY: docker-rebuild-env

docker-build-env-local:
	docker-compose -f docker-compose.local.yml up --remove-orphans
.PHONY: docker-build-env-local

docker-shell-env:
	docker-compose exec email-gateway /bin/bash
.PHONY: docker-shell-env

docker-stop:
	docker-compose down --volumes
	docker-compose rm -f
.PHONY: docker-stop

docker-run:
	python -m email_gateway
.PHONY: docker-run

send-email:
	python scripts/send-email.py
.PHONY: send-email
