"""Utility to send one email inside the docker environment."""

import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

sender_email = "noreply@cern.ch"
receiver_email = "notificationsdv+my-channel+normal@cern.ch"

message = MIMEMultipart()
message.attach(MIMEText("This is a test e-mail message.", 'plain'))

message['Subject'] = 'Our normal e-mail test'
message['From'] = "Person <noreply@cern.ch>"
message['To'] = "<notificationsdv+my-channel+normal@cern.ch>"
message['Date'] = "Wed, 10 Aug 2022 11:27:24 +0200"
message['Delivered-To'] = "notificationsdv+test-wa-caetan+low"

with smtplib.SMTP(os.getenv("SERVER")) as server:
    server.send_message(message, sender_email, [receiver_email])
